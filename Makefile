CC=c99
CFLAGS=-g -std=c99 -DNCURSES_WIDECHAR=1
PREFIX=/usr/local

all: cfselect 

cfselector.o: cfselector.c cfselector.h
	$(CC) $(CFLAGS) -c cfselector.c
cfselect.o: cfselect.c cfselector.h
	$(CC) $(CFLAGS) -c cfselect.c

cfselect: cfselect.o cfselector.o
	$(CC) $(CFLAGS) cfselector.o cfselect.o -o cfselect -lncursesw

clean:
	rm -f *.o cfselect

install: cfselect
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -pf cfselect $(DESTDIR)$(PREFIX)/bin