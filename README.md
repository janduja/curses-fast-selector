## About
This curses program opens a screen for fast selection of an item in a list.

![cfselector preview](http://janduja.com/cfselector/cfselect.gif "cfselector preview")

Once opened, the full list of items appears and that is filtered as the user types in any search criteria.
The filter box is editable and will filter the list (case insensitive) by those lines that contains all of the parts of the input text (separated by spaces or tabs). 

Documentation from the header source file:
```c
/*
 This function open a screen for fast selection of an item from a list.
 The items is a an array of pointers to generic items which must be
 terminated by a NULL element. The item_description is a function that
 is executed on each item to get its description as a string.
 It returns the item selected, or NULL if none was choosen.
*/
void* cfselector(WINDOW* win, char* title, void** items, char* (item_description)(void*));
```

An executable program is also provided that takes the list from a file and writes the choice to another file.
This can be useful for example if the user wants to provide an interactive named pipe in his scripts.

Source code is standard C99 and should be compilable in any POSIX environment. It was tested on Linux, adaptation to BSD and other systems should be straightforward.

## Installation of the Executable
Run ```make``` to compile, then run ```make install``` as root user to install the executable.

Default program location will be ```/usr/local/bin/cfselect```, a custom destination directory for the executable can be specified by setting variable DESTDIR while building with make.

## Example Usage of the Executable
Given a list of items in a file ```list.txt```, the following command will open a screen with title ```Provide your selection from below...``` and
the choice will be written to a file ```result.txt```:
```
$ cfselect list.txt result.txt "Provide your selection from below..."
```
The title (third parameter) is optional.

## License
[The MIT License (MIT)](http://opensource.org/licenses/mit-license.php)
