/*
 Copyright (c) 2018 Raffaele Arecchi <raffaele@arecchi.eu>
  
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
        
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
         
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <stdlib.h>
#include <wchar.h>
#include <wctype.h>
#include <ctype.h>
#include <ncursesw/ncurses.h>
#include <string.h>
#include "cfselector.h"

#define TAB_SIZE 4

static int match(char** filters, int filters_size, void* item, char* (item_description)(void*))
{
	char* desc= item_description(item);
	char lowered [strlen(desc)];
	for (int i=0; i<strlen(desc); i++)
		*(lowered+i) = tolower(*(desc+i));
	for (int i=0; i<filters_size; i++)
	{
		if (strstr(lowered, *(filters+i)) == NULL)
			return 0;
	}
	return 1;
}

static char** convert_to_tokens(cchar_t** filter, int filter_size, int* filter_chars_size)
{
	wchar_t filter_wc [filter_size];
	for (int i=0; i<filter_size; i++)
	{
		cchar_t* c = *(filter+i);
		wchar_t wc = *(c->chars);
		char buffer [MB_CUR_MAX];
		int l = wctomb(buffer, wc);
		if (l==1 && *buffer == '\t')
			*(filter_wc+i) = L' ';
		else
			*(filter_wc+i) = towlower(*(c->chars));
	}
	char str[filter_size];
	wcstombs(str, filter_wc, filter_size);
	
	char str2[filter_size];
	for (int i=0; i<filter_size; i++)
		*(str2+i) = *(str+i);
	int tokens = 0;
	char *token = strtok(str, " ");
    while (token != NULL)
	{	
		tokens++;
		token = strtok(NULL, " ");
    }
	*filter_chars_size = tokens;
	char** res = malloc(tokens * sizeof(char*));
	for (int i=0; i<tokens; i++)
	{
		if (i==0)
			token = strtok(str2, " ");
		else
			token = strtok(NULL, " ");
		int len_tok = strlen(token);
		char* val = malloc((len_tok + 1)* sizeof(char));
		*(val+len_tok) = '\0';
		for (int j=0; j<strlen(token);j++)
			*(val+j) = *(token+j);
		*(res+i) = val;
	}
	return res;
}

static void** build_result_from_filter(cchar_t** filter, int filter_size, void** items, char* (item_description)(void*))
{
	size_t size = 0;
	size_t capacity = 8;
	int filter_chars_size = 0;
	char** filter_chars = convert_to_tokens(filter, filter_size, &filter_chars_size);
	void** result = (void**) calloc(capacity, sizeof(void**));
	while (*items != NULL)
	{
		if (match(filter_chars, filter_chars_size, *items, item_description))
		{
			if (size == capacity)
			{
				capacity += 8;
				result = realloc(result, capacity * sizeof(void**));
			}
			result[size] = *items;
			size++;
		}
		items++;
	}
	for (int i=0; i<filter_chars_size; i++)
		free(filter_chars[i]);
	free(filter_chars);
	// set last pointer to NULL
	if (size == capacity)
	{
		capacity += 1;
		result = realloc(result, capacity * sizeof(void**));
	}
	result[size] = NULL;
	return result;
}

static void print_result(void** items, int selected_item, char* (item_description)(void*), int maxy)
{
	int y = 4;

	// cleaning screen
	for (int i=4; i<=maxy; i++)
	{
		move(i, 0);
		clrtoeol();
	}

	int	counter = 0;
	while (*items != NULL)
	{
		move(y, 0);
		if (counter == selected_item)
			printw("=> ");
		else
			printw("   ");
		printw(item_description(*items));
		items++;
		counter++;
		y++;
		if (y>maxy)
			break;
	}
}

static void insert_element(void** arr, void* element, int position, int length)
{
	free(arr[length-1]);
    for (int i=length-1; i >= 0; i--)
    {	
		if (i <= position)
			continue;
		else 
	        arr[i] = arr[i-1];
    }
	arr[position] = element;
}

static void remove_element(cchar_t** arr, int position, int length)
{
	free(arr[position]);
    for (int i=0; i<length-1; i++)
    {	
		if (i < position)
			continue;
		else 
			arr[i] = arr[i+1];
    }
	cchar_t* ch = malloc(sizeof(cchar_t));
	ch->attr = A_NORMAL;
	ch->chars[0] = ' ';
	ch->chars[1] = '\0';
	arr[length-1] = ch;
}

static void reprint_filter_line(cchar_t** filter_buffer, int cursor_position, int filter_size)
{
	move(2, 0);
	clrtoeol();
	printw("Type anything to filter: ");
	int tabs_before_curs_position = 0;
	for(int i=0; i<filter_size; i++)
	{
		if (filter_buffer[i]->chars[0] == '\t')
		{
			for(int j=0; j<TAB_SIZE; j++)
				addch(' ');
			if (i<cursor_position)
			tabs_before_curs_position++;
		}
		else
			add_wch(filter_buffer[i]);
	}
	move(2, 25 + cursor_position + tabs_before_curs_position*(TAB_SIZE-1));
	refresh();
}

static int last_position_non_blank(cchar_t** filter_buffer, int filter_size)
{
	int pos = -1;
	for(int i=0; i<filter_size; i++)
	{
		if(filter_buffer[i]->chars[0] != ' ')
			pos = i;
	}
	return pos;
}

void* cfselector(WINDOW* win, char* title, void** items, char* (item_description)(void*))
{
	int filter_size = 100;
	int maxx, maxy;
	void** result;
	int selected_item;
	cchar_t* filter_buffer[filter_size];
	int cursor_position;
	
	for(int i=0; i<filter_size; i++)
	{
		filter_buffer[i] = malloc(sizeof(cchar_t));
		filter_buffer[i]->attr = A_NORMAL;
		filter_buffer[i]->chars[0] = ' ';
		filter_buffer[i]->chars[1] = '\0';
	}
	getmaxyx(win, maxy, maxx);
	clear();
	move(0, 0);
	attron(A_REVERSE);
	for (int i=0; i < maxx; i++)
		addch(' ' | A_REVERSE);
	move(0, 0);
	printw("[F1] leave, [Up/Down] select item, [Enter] choose item."); 
	move(1, 0);
	attroff(A_REVERSE);
	printw(title);
	move(2, 0);
	printw("Type anything to filter: ");
	cursor_position = 0;
	result = build_result_from_filter(filter_buffer, filter_size, items, item_description);
	selected_item = 0;
	print_result(result, selected_item, item_description, maxy);
	move(2,25);
	refresh();
	void* choice;
	while (true)
	{
		wint_t ch;
		int gch = get_wch(&ch);
		if (gch == ERR || ch == KEY_F(1))
		{
			choice = NULL;
			break;
		}
		if (ch == KEY_DOWN)
		{
			if (result[selected_item+1] != NULL)
			{
				selected_item++;
				print_result(result, selected_item, item_description, maxy);
				reprint_filter_line(filter_buffer, cursor_position, filter_size);
				refresh();
			}
			continue;
		}
		if (ch == KEY_UP)
		{
			if (selected_item!=0)
			{
				selected_item--;
				print_result(result, selected_item, item_description, maxy);
				reprint_filter_line(filter_buffer, cursor_position, filter_size);
				refresh();
			}
			continue;
		}
		if (ch == '\n' || ch == KEY_ENTER)
		{
			if (result != NULL)
				return *(result+selected_item);
			continue;
		}
		if (ch == KEY_BACKSPACE)
		{
			if (cursor_position > 0)
			{
				remove_element(filter_buffer, cursor_position-1, filter_size);
				cursor_position--;
				free(result);
				result = build_result_from_filter(filter_buffer, filter_size, items, item_description);
				print_result(result, selected_item, item_description, maxy);
				reprint_filter_line(filter_buffer, cursor_position, filter_size);
			}
			continue;
		}
		if (ch == KEY_DC)
		{
			remove_element(filter_buffer, cursor_position, filter_size);
			free(result);
			result = build_result_from_filter(filter_buffer, filter_size, items, item_description);
			print_result(result, selected_item, item_description, maxy);
			reprint_filter_line(filter_buffer, cursor_position, filter_size);
			refresh();
			continue;
		}
		if (ch == KEY_LEFT)
		{
			if (cursor_position > 0)
			{
				cursor_position--;
				reprint_filter_line(filter_buffer, cursor_position, filter_size);
			}
			continue;
		}
		if (ch == KEY_RIGHT)
		{
			if (cursor_position < filter_size && cursor_position <= last_position_non_blank(filter_buffer, filter_size))
			{
				cursor_position++;
				reprint_filter_line(filter_buffer, cursor_position, filter_size);
			}
			continue;
		}
		if (ch == KEY_HOME && cursor_position > 0)
		{
			cursor_position = 0;
			reprint_filter_line(filter_buffer, cursor_position, filter_size);
			continue;
		}
		if (ch == KEY_END)
		{
			cursor_position = last_position_non_blank(filter_buffer, filter_size)+1;
			if (cursor_position==filter_size)
				cursor_position--;
			reprint_filter_line(filter_buffer, cursor_position, filter_size);
			continue;
		}
		if (cursor_position >= filter_size)
			continue;
		// any other case insert character in filter buffer
		cchar_t* cch = malloc(sizeof(cchar_t));
		cch->attr = A_NORMAL;
		cch->chars[0] = ch;
		cch->chars[1] = '\0';
		insert_element((void**)filter_buffer, cch, cursor_position, filter_size);
		cursor_position++;
		free(result);
		result = build_result_from_filter(filter_buffer, filter_size, items, item_description);
		selected_item = 0;
		print_result(result, selected_item, item_description, maxy);
		reprint_filter_line(filter_buffer, cursor_position, filter_size);
		refresh();
	}
	for(int i=0; i<filter_size; i++)
		free(filter_buffer[i]);
	free(result);
	return choice;
}