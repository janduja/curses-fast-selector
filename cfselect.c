/*
 Copyright (c) 2018 Raffaele Arecchi
  
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
        
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
         
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <locale.h>
#include <wchar.h>
#include <ncursesw/curses.h>
#include "cfselector.h"

static char * getline(FILE* fp) {
    char * line = malloc(100), * linep = line;
    size_t lenmax = 100, len = lenmax;
    int c;

    if(line == NULL)
        return NULL;

    for(;;) {
        c = fgetc(fp);
        if(c == EOF)
            break;

        if(--len == 0) {
            len = lenmax;
            char * linen = realloc(linep, lenmax *= 2);

            if(linen == NULL) {
                free(linep);
                return NULL;
            }
            line = linen + (line - linep);
            linep = linen;
        }

        if((*line++ = c) == '\n')
            break;
    }
    *line = '\0';
    return linep;
}

static char* item_desc(void* item)
{
	return (char*) item;
}

int main(int argc, char *argv[])
{
	if (argc < 3)
		exit(EXIT_FAILURE);
	char* title = "Select a choice from the list below";
	if (argc > 3)
		title = argv[3];

	FILE * fp;
	fp = fopen(argv[1], "r");
	if (fp == NULL)
		exit(EXIT_FAILURE);
	        
	size_t capacity = 8;
	
	char** lines = malloc(capacity * sizeof(char**));
	int i = 0;
	char* line = getline(fp);
	while (*line != '\0')
	{
		if (capacity == i+1)
		{
			capacity += 8;
		 	lines = realloc(lines, capacity * (sizeof(char**)));
		}
		*(lines+i) = line;
		i++;
		line = getline(fp);
	}
	fclose(fp);
	if (capacity == i+1)
	{
		capacity += 1;
	 	lines = realloc(lines, capacity * (sizeof(char**)));
	}
	*(lines+i)=NULL;
	
	setlocale(LC_ALL,"");
	WINDOW* win = initscr();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);


	void* selected_item = cfselector(win, title, (void**) lines, item_desc);
	endwin();
	
	if(selected_item!=NULL)
	{
	    FILE* fp2 = fopen(argv[2], "w");
	    if (fp2 == NULL)
			exit(EXIT_FAILURE);
		fprintf(fp2, item_desc(selected_item));
		fclose(fp2);
	}
	for (int k=0; k<=i; k++)
		free(*(lines+k));
	free(lines);
	return 0;
}
